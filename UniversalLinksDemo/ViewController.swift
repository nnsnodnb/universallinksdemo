//
//  ViewController.swift
//  UniversalLinksDemo
//
//  Created by Oka Yuya on 2018/07/11.
//  Copyright © 2018年 Yuya Oka. All rights reserved.
//

import UIKit

let openUniversalLinksNotification = Notification.Name("openUniversalLinksNotification")

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Launch normal."
        NotificationCenter.default.addObserver(self, selector: #selector(openUniversalLinks(_:)), name: openUniversalLinksNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: openUniversalLinksNotification, object: nil)
    }

    // MARK: - Notification selector

    @objc private func openUniversalLinks(_ notification: Notification) {
        DispatchQueue.main.async {
            self.label.text = "Open by UniversalLinks😎"
        }
    }
}
